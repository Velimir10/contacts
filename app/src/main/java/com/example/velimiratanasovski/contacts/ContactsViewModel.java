package com.example.velimiratanasovski.contacts;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import java.util.List;
import com.example.velimiratanasovski.contacts.db.ContactRepository;
import com.example.velimiratanasovski.contacts.model.Contact;

public class ContactsViewModel extends AndroidViewModel {


    private LiveData<List<Contact>> mContacts;
    private ContactRepository mContactRepository;

    public ContactsViewModel(@NonNull Application application) {
        super(application);
        mContactRepository = new ContactRepository(application);
        mContacts = mContactRepository.getAllContactsList();
    }

    public LiveData<List<Contact>> getContacts() {
        return mContacts;
    }
    public void insert(Contact contact){
        mContactRepository.insert(contact);
    }

}
