package com.example.velimiratanasovski.contacts.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import java.util.List;
import com.example.velimiratanasovski.contacts.model.Contact;

public class ContactRepository {

    private ContactDao mContactDao;
    private LiveData<List<Contact>> mAllContactsList;

    public ContactRepository(Application application) {
        ContactRoomDatabase db = ContactRoomDatabase.getInstance(application);
        mContactDao = db.contactDao();
        mAllContactsList = mContactDao.getAllContacts();
    }

    public LiveData<List<Contact>> getAllContactsList() {
        return mAllContactsList;
    }

    public void insert(Contact contact) {
        new insertAsyncTask(mContactDao).execute(contact);
    }

    public static class insertAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDao mAsyncDao;

        public insertAsyncTask(ContactDao dao) {
            mAsyncDao = dao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            mAsyncDao.insertContact(contacts[0]);
            return null;
        }
    }

}
