package com.example.velimiratanasovski.contacts;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.List;
import com.example.velimiratanasovski.contacts.adapters.MyContactRecyclerAdapter;
import com.example.velimiratanasovski.contacts.model.Contact;
import static com.example.velimiratanasovski.contacts.AddContactActivity.NEW_CONTACT;
import static com.example.velimiratanasovski.contacts.DetailContactActivity.CONTACT_POSITION;


public class MainActivity extends AppCompatActivity implements MyContactRecyclerAdapter.ItemClickListener {

    public static final int ADD_ACTIVITY_REQUEST_CODE = 0;
    private RecyclerView mRecyclerView;
    private MyContactRecyclerAdapter mAdapter;
    private ContactsViewModel mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mViewModel = ViewModelProviders.of(this).get(ContactsViewModel.class);
        mAdapter = new MyContactRecyclerAdapter(this, this);
        mViewModel.getContacts().observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable List<Contact> contacts) {
                if (mAdapter != null) {
                     mAdapter.setContacts(contacts);
                }

            }
        });
        mRecyclerView.setAdapter(mAdapter);


    }

    public void onFabClick(View view) {

        Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
        startActivityForResult(intent, ADD_ACTIVITY_REQUEST_CODE);

    }


    // This method is called when 2nd Activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ADD_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                Contact newContact = data.getExtras().getParcelable(NEW_CONTACT);
                if (newContact != null) {
                    mViewModel.insert(newContact);

                }
            }
        }
    }


    @Override
    public void OnItemClick(Contact contact) {
        Intent intent = new Intent(this, DetailContactActivity.class);
        intent.putExtra(CONTACT_POSITION, contact);
        startActivity(intent);
    }
}
