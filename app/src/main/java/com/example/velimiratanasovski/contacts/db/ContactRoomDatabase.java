package com.example.velimiratanasovski.contacts.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import com.example.velimiratanasovski.contacts.model.Contact;

@Database(entities = Contact.class, version = 2)
public abstract class ContactRoomDatabase extends RoomDatabase {

    private static ContactRoomDatabase sInstance;

    public abstract ContactDao contactDao();

    public static ContactRoomDatabase getInstance(final Context context){

        if(sInstance == null){
            synchronized (ContactRoomDatabase.class){
                if(sInstance == null){
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            ContactRoomDatabase.class, "contact_database")
                            .addCallback(sRoomDatabaseCallback).build();
                }
            }
        }
        return sInstance;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);
                    new AddInitialContacts(sInstance.contactDao()).execute();
                }

            };

    public static class AddInitialContacts extends AsyncTask<Void, Void, Void> {

        private ContactDao mAsyncDao;

        public AddInitialContacts(ContactDao dao) {
            mAsyncDao = dao;
        }

        @Override
        protected Void doInBackground(Void... params) {

            mAsyncDao.insertContact(new Contact("Marko", "Markovic",
                    "Heroja Maricica 90", "5343434"));
            mAsyncDao.insertContact(new Contact("Stefan", "Stefanovic",
                    "Milutina Milankovica 86", "0000000"));
            mAsyncDao.insertContact(new Contact("Velimir", "Atanasovski",
                    "Hajduk Veljkova 9", "5555555"));
            mAsyncDao.insertContact(new Contact("Djordje", "Djordjevic",
                    "Heroja Maricica 909", "1111111"));
            mAsyncDao.insertContact(new Contact("Marko", "Markovic",
                    "Heroja Maricica 90", "222222"));
            mAsyncDao.insertContact(new Contact("Milutin", "Milutinovic",
                    "Heroja Maricica 90", "2234244"));
            mAsyncDao.insertContact(new Contact("Milos", "Maksimovic",
                    "Djordja Perovica 7", "6666666"));
            mAsyncDao.insertContact(new Contact("Bogdan", "Bogdanovic",
                    "Bulevar Umetnosti", "234234234"));
            mAsyncDao.insertContact(new Contact("Marko", "Lazovic",
                    "Bulevar Umetnosti 19", "2423423"));
            mAsyncDao.insertContact(new Contact("Lazar", "Djokic",
                    "Cara Lazara 2", "2344239"));
            return null;
        }
    }
}
