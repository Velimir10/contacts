package com.example.velimiratanasovski.contacts.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;
import com.example.velimiratanasovski.contacts.model.Contact;

@Dao
public interface ContactDao {

    @Insert
    void insertContact(Contact contact);

    @Query("SELECT * FROM contact_table ORDER BY name COLLATE NOCASE ASC")
    LiveData<List<Contact>> getAllContacts();

}
